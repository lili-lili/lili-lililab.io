In order to make edits on my website, I had to make a clone on my laptop first. To do so first I downloaded a software name sourcetree, which is a Git GUI that simplifies the editing process with Git repositories. link:https://www.sourcetreeapp.com/

{{< toc >}}

Open SourceTree then from tools choose the generate or import SSH keys option. then Click on generate. - just a tip: don't leave it and go elsewhere as you have to move your mouse randomly so it can generate it -

![imgxdd](/media/pic3.PNG) 

{{< toc >}}

Once keys are generated, copy the "public key" and paste it in your Gitlab account. -if you messed up this step and will repeat it, don't forget to delete the previous key you entered-

![imgyyuy](/media/4th.PNG) 

{{< toc >}}

Then save the public and private keys somewhere save, we will use them shortly.

![imgqwq](/media/pic5.PNG) 

{{< toc >}}

Back to sourcetree from tools-options then add your "private SSH key".

![imglp](/media/6th.PNG) 

{{< toc >}}

then go to your project and click on clone then clone with SSH, the link you will get you will paste it in sourcetree in clone

![imgluup](/media/7th.PNG) 
![imgwwlp](/media/8th.PNG) 

{{< toc >}}

AAnnndd, finally!

![imgrlp](/media/9th.png) 

{{< toc >}}

