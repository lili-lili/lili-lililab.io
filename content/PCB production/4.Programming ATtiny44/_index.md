Programming ATtiny44

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->

Now that we have a soldered PCB, It's now ready to be programmed!


First, download Arduino IDE from: https://www.arduino.cc/en/software . Then open it and from file > prefrence > add this link to additional boards: https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json

![img](/media/d1.PNG)

Then from tools > boards > board manager > type attiny then download this:

![img](/media/d2.PNG)

Then select the board as ATtiny44 / clock: internal 8MHZ  / and Atmel-ICE - AVR : as our programmer

![img](/media/d3.png)

Now, Arduino IDE is fully configured and we are ready to upload our code into the microcontroller. To successfully upload the code we need to do 2 things:


* Connect Atmel-ICE with ISP 6-pins like the next photo.

![img](/media/d4.PNG)
![img](/media/d44.PNG)

* Connect FTDI cable to our board and the PC's USB "black wire with GND "

![img](/media/d5.PNG)

At first I had an error so I checked device manager to make sure that Atmel-ICE is connected successfully with my laptop, but it needed installing a driver so it first looked like this:

![img](/media/e1.PNG)

Then I googled Atmel-ICE driver and downloaded driver hub desktop app which installed it for me so when I connected it again the laptop read Atmel-ICE as it is.

![img](/media/e2.PNG)


If any errors appears in Arduino IDE after this it probably be the wrong connection of ISP 6-wire connection or the FTDI cable not working as it should so triple check on those 2!


Now, it's all set. Open demo.ino code then upload it to the board. And yaaay it worked! enjoy seeing those LEDs dance!

[![Dancing LEDs](https://res.cloudinary.com/marcomontalbano/image/upload/v1640644471/video_to_markdown/images/google-drive--1WHG8ebKzvdQu8xVAaEcsypY7VBfQaEmE-c05b58ac6eb4c4700831b2b3070cd403.jpg)](https://drive.google.com/file/d/1WHG8ebKzvdQu8xVAaEcsypY7VBfQaEmE/view?usp=sharing "Dancing LEDs")
