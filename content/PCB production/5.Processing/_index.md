Processing

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->

Our last step is to control the LEDs on the board using processing


First, download Processing from:https://processing.org/download. I downloaded processing 3.5.4 


Then we need to edit the arduino code a bit, I uncommented 2 lines and commented dance function in the code to be able to make processing control which LED turns on and off through serial port. Then upload the code again with same connection and configuration.


![img](/media/p1.PNG)

After programming the ATtiny with the new code we no more need Atmel-ICE so disconnect it and the ISP 6-wire pins, just keep the FTDI cable connecting your board with the laptop.Now, open processing IDE and open "led pattern" code. you may need to change line no.50 according to the port the FTDI is connected with, put the original [0] in the code worked with me from the first time.


Now you are ready to hit RUN button, the next window will show up.

![img](/media/p2.PNG)

to make the LED turn on or off press any light-grey button then press the dark-grey one, and TAA DAA! 

[![Processing controls LEDs](https://res.cloudinary.com/marcomontalbano/image/upload/v1640644551/video_to_markdown/images/google-drive--1xg6gay1E8iFzfw0UePd2TUT9U82JfuQn-c05b58ac6eb4c4700831b2b3070cd403.jpg)](https://drive.google.com/file/d/1xg6gay1E8iFzfw0UePd2TUT9U82JfuQn/view?usp=sharing "Processing controls LEDs")

