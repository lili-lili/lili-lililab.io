Milling on Modela MDX-20

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->

---

Now that our two images are ready we need to send them to Modela MDX-20 to make its magic and fabricate our PCB. So, to be able to do so we first need to connect with modela via our PC so we start terminal and type :" cd fab " which means change directory - go to file named fab. Then type " sudo npm start " which starts a program named npm, it will need to confirm that you are an administrator so it will request PC's password then it will connect with the modela like the photo:

![img](/media/b1.png)

Then open firefox browser, you will find a tab that has the same IP address written in terminal. it will automatically open Fabmodules website which is an online tool that generates G-Code for all CNC machines in a Fab Lab. you will need to fill some data like the next picture:

![img](/media/b2.png)

It needs a little demonstration so here is the color code :D 

* Yellow: is inputs you need to enter. so first choose the file type so i choose .png, and then for the output file I choose Roland mill " .rml file " and lastly process > for traces we choose traces 1/64 which is in inches and in mm it's 0.4mm which is the diameter of our end mill.
* Red: it should be the same resolution we entered in Gimp software so it's 1000
* Orange: here we choose the machine we are using so I choose modela mdx-20
* Pink: those are the coordinates that you want the machine to move to - you can't move z-axis from here, you do it manually from the maching -
* light-green: those are the home coordinates, don't change them as they are automatically filled when you enter the machine you are using.
* Purple: those are the cut settings that greatly affect the G-code. Cut depth: is how much the tool will go into the PCB in z-axis. tool diameter: is the fiameter of your end mill. offest: is how many itterations you want the machine to mill around each trace, the more the easier it will be while soldering - 3 or 4 times are good -.

After setting those inputs hit calculate to generate your G-code, It should look like this:

![img](/media/b3.png)

Now that we are ready to send our file we need to prepare our machine. You will need a PCB sheet, double face, tape and a cutter or scissors. Flip the PCB sheet then first put the tape and then the double face then remove any excess with your cutter.

![img](/media/b4.PNG)
![img](/media/b5.PNG)

Now remove the double face and but it on Modela's bed and push it well against it to make sure it won't move while machining. Then open the modela from the ON/OFF switch inside the white area. 

![img](/media/b6.PNG)

When the machines starts it switches to viewing mode on the right, to cancel this mode you need to click on the view button inside the light-blue area. in view mode it's best to put your end mill into the collet. Then to control your z-axis manually you press up and down buttons in blue and yellow areas.  In this PCB we will use 2 end mills. 0.4mm V-bit for traces and 0.8mm end mill for drills and outline.

![img](/media/b7.PNG)

After tightening the V-bit on the collet we are ready to run our file, so from fab modules click send. If everything goes well, you will watch the modela removing just the right amount of copper off the PCB sheet, like this:

![img](/media/b8.PNG)

After traces file finishies you will enjoy this beautiful view.

![img](/media/b9.PNG)

Then we will repeat the process for the outline, choose outline image and it all be same inputs except for process we will choose outline 1/32 in inches which is 0.8 in mm which is the diameter of our end mill. If everything goes as expected it will finish successfully like this:

![img](/media/b10.PNG)





