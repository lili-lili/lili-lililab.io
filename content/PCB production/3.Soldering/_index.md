Soldering

<!-- spellchecker-disable -->

{{< toc-tree >}}

<!-- spellchecker-enable -->

Now that our PCB is ready, we need to solder it with components, I used ATtiny44, 6 smd LEDs, 3 499 ohms resistors, 1 10k resistor, 1 1µF capacitor, 0.1µF capacitor and pin headers.

![img](/media/c1.PNG)

To start soldeing we need a soldering iron, solder, flux, tweezer to hold those tiny SMD components, and AVO meter so we could use it in buzzer mode to check if there is any short-circuit happened.

![img](/media/c2.PNG)

I watched some very helpful tutorials of how to solder SMD components then started, I soldered from the middle out.

![img](/media/c3.PNG)

Soldering SMD compnents can be done using different techniques. I used on where we:

* put solder on a pad.
* put the component we need to solder.
* heat that pad with our soldering iron so that solder pins down the component.
* now that the comonnet is fixed into its place we solder its other pins and also the first pin.


[![](https://res.cloudinary.com/marcomontalbano/image/upload/v1640483237/video_to_markdown/images/youtube--AAHfTO73nr8-c05b58ac6eb4c4700831b2b3070cd403.jpg)](https://www.youtube.com/watch?v=AAHfTO73nr8 "")

After doning so here is how my PCB looked like:

![img](/media/c4.PNG)